package com.atlassian.functest.spring;

import com.atlassian.functest.junit.SpringAwareTestCase;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class SpringTest extends SpringAwareTestCase {
    @ComponentImport
    private PluginAccessor pluginAccessor;

    public void setPluginAccessor(PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    @Test
    public void injectionWorks() {
        assertThat(pluginAccessor, notNullValue());
    }
}
