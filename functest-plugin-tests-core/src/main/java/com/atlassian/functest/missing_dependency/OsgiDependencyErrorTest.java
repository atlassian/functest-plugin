package com.atlassian.functest.missing_dependency;

import com.atlassian.functest.junit.SpringAwareTestCase;
import com.atlassian.functest.web.util.RendererContextBuilder;
import org.junit.Test;

public class OsgiDependencyErrorTest extends SpringAwareTestCase {

    // this method will prevent the class to be loaded
    // because RendererContextBuilder is not available
    public RendererContextBuilder method() {
        return null;
    }

    @Test
    public void neverCalled() {
    }
}
