package com.atlassian.functest.it;

import com.atlassian.functest.error.ErrorTest;
import com.atlassian.functest.failing.FailingTest;
import com.atlassian.functest.inject.InjectErrorTest;
import com.atlassian.functest.rest.TestGroup;
import com.atlassian.functest.rest.TestResults;
import com.atlassian.functest.spring.SpringTest;
import com.atlassian.functest.test.ExcludedTest;
import com.atlassian.functest.test.JUnit3Test;
import com.atlassian.functest.test.SimpleTest;
import com.atlassian.functest.test.SystemPropertyTest;
import com.atlassian.functest.test2.AnotherSimpleTest;
import com.atlassian.functest.test3.AbstractTestClass;
import com.atlassian.functest.test3.AndAnotherSimpleTest;
import com.atlassian.functest.test3.NonTestClass;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.io.File;
import java.net.URI;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class RestTestBase {
    private static final String PORT = System.getProperty("http.port", "5990");
    private static final String CONTEXT = System.getProperty("context.path", "/refapp");

    @Before
    public void setUp() {
        URI uri = UriBuilder.fromUri("http://localhost/").port(Integer.parseInt(PORT)).path(CONTEXT)
                .path("rest").path("functest").path("1.0").path("junit").path("systemProperty").build();
        ClientBuilder.newClient().target(uri).queryParam("functestSystemProperty", "true").request().post(null);
    }

    @After
    public void tearDown() throws Exception {
        FileUtils.deleteDirectory(new File("target/runtest"));
    }

    @Test
    public void testJsonPost() {
        URI uri = UriBuilder.fromUri("http://localhost/").port(Integer.parseInt(PORT)).path(CONTEXT)
                .path("rest").path("functest").path("1.0").path("junit").path("systemProperty").build();
        ClientBuilder.newClient()
                .target(uri)
                .queryParam("functestSystemProperty", "true")
                .request(MediaType.APPLICATION_JSON)
                .post(null);
    }

    @Test
    public void testJsonGet() {
        URI uri = UriBuilder.fromUri("http://localhost/").port(Integer.parseInt(PORT)).path(CONTEXT)
                .path("rest").path("functest").path("1.0").path("junit").path("getTests")
                .queryParam("groups", "foo")
                .build();
        TestGroup[] groups = getTestGroups(uri);
        assertTrue(groups.length > 0);
    }

    @Test
    public void testGetTestsForSpring() {
        URI uri = UriBuilder.fromUri("http://localhost/").port(Integer.parseInt(PORT)).path(CONTEXT)
                .path("rest/functest/1.0/junit/getTests")
                .queryParam("groups", "spring")
                .build();
        TestGroup[] groups = getTestGroups(uri);

        assertThat(groups.length, equalTo(1));
        assertThat(groups[0].getTests().size(), is(3));
    }

    @Test(expected = InternalServerErrorException.class)
    public void testGetAllTestsFails() {
        URI uri = UriBuilder.fromUri("http://localhost/").port(Integer.parseInt(PORT)).path(CONTEXT)
                .path("rest/functest/1.0/junit/getTests")
                .build();
        getTestGroups(uri);

        fail("Request to getTests() should fail as OsgiDependencyErrorTest cannot be instantiated");
    }

    private TestGroup[] getTestGroups(URI uri) {
        return ClientBuilder.newClient()
                .target(uri)
                .request(MediaType.APPLICATION_JSON)
                .get(TestGroup[].class);
    }

    @Test
    public void run() {
        File targetDir = new File("target");
        URI uri = UriBuilder.fromUri("http://localhost/").port(Integer.parseInt(PORT)).path(CONTEXT)
                .path("rest").path("functest").path("1.0").path("junit").path("runTests")
                .build();
        String [] safeGroups = {"foo", "foo2", "failing", "error", "inject", "spring"};
        WebTarget webResource = ClientBuilder.newClient()
                .target(uri)
                .queryParam("groups", (Object[]) safeGroups)
                .queryParam("outdir", targetDir.getAbsolutePath())
                .queryParam("excludes", FailingTest.class.getName())
                .queryParam("excludes", ErrorTest.class.getName())
                .queryParam("excludes", InjectErrorTest.class.getName());
        TestResults results = webResource.request().get(TestResults.class);
        assertThat(results, notNullValue());
        assertThat(results.result, is(0));
        assertTrue(new File(targetDir, "foo-reports/" + SimpleTest.class.getName() + ".txt").exists());
        assertTrue(new File(targetDir, "foo-reports/TEST-" + SimpleTest.class.getName() + ".xml").exists());
        assertTrue(new File(targetDir, "foo-reports/" + JUnit3Test.class.getName() + ".txt").exists());
        assertTrue(new File(targetDir, "foo-reports/TEST-" + JUnit3Test.class.getName() + ".xml").exists());
        assertTrue(new File(targetDir, "foo-reports/" + SystemPropertyTest.class.getName() + ".txt").exists());
        assertTrue(new File(targetDir, "foo-reports/TEST-" + SystemPropertyTest.class.getName() + ".xml").exists());
        assertFalse(new File(targetDir, "foo-reports/" + ExcludedTest.class.getName() + ".txt").exists());
        assertFalse(new File(targetDir, "foo-reports/TEST-" + ExcludedTest.class.getName() + ".xml").exists());

        assertTrue(new File(targetDir, "foo2-reports/" + AnotherSimpleTest.class.getName() + ".txt").exists());
        assertTrue(new File(targetDir, "foo2-reports/TEST-" + AnotherSimpleTest.class.getName() + ".xml").exists());
        assertTrue(new File(targetDir, "foo2-reports/" + AndAnotherSimpleTest.class.getName() + ".txt").exists());
        assertTrue(new File(targetDir, "foo2-reports/TEST-" + AndAnotherSimpleTest.class.getName() + ".xml").exists());

        assertFalse(new File(targetDir, "foo2-reports/" + NonTestClass.class.getName() + ".txt").exists());
        assertFalse(new File(targetDir, "foo2-reports/TEST-" + NonTestClass.class.getName() + ".xml").exists());
        assertFalse(new File(targetDir, "foo2-reports/" + AbstractTestClass.class.getName() + ".txt").exists());
        assertFalse(new File(targetDir, "foo2-reports/TEST-" + AbstractTestClass.class.getName() + ".xml").exists());

        assertTrue(new File(targetDir, "spring-reports/" + SpringTest.class.getName() + ".txt").exists());
        assertTrue(new File(targetDir, "spring-reports/TEST-" + SpringTest.class.getName() + ".xml").exists());
    }
}
