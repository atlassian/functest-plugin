Introduction
============
This file contains information useful to developers who want to contribute to this project.

Running
=======
To run just the `functest-plugin`:
1. `mvn clean install -DskipTests`
1. `cd functest-plugin`
1. `mvn amps:debug -DskipTests`
1. point your browser to http://localhost:5990/refapp/plugins/servlet/upm/manage/all; the "Functional Test Plugin" and
its four modules should all be enabled.
1. point your browser to http://localhost:5990/refapp/rest/functest/1.0/junit/getTests; you should get a response like
this:
```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<testGroups>
    <testGroup>
        <name>streams-scheduler-test</name>
    </testGroup>
</testGroups>
```

To run the `functest-plugin`'s own tests:
1. `mvn clean install -DskipTests`
1. `cd functest-plugin-tests` (this is a plugin too)
1. `mvn amps:debug -DskipTests`
1. etc.
