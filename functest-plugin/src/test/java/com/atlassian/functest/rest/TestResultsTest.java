package com.atlassian.functest.rest;

import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;

/**
 * Tests that the production code has the dependencies necessary to marshal/unmarshal {@link TestResults} instances to
 * and from XML.
 */
public class TestResultsTest {

    private JAXBContext jaxbContext;

    @Before
    public void setUpJaxbContext() throws JAXBException {
        jaxbContext = JAXBContext.newInstance(TestResults.class);
    }

    @Test
    public void instance_whenRoundTrippedToXml_shouldContainSameValues() throws Exception {
        // Set up
        final String outdir = "theOutdir";
        final String output = "theOutput";
        final int result = 42;
        final TestResults testResultsIn = new TestResults(result, output, outdir);

        // Invoke
        final String xml = serializeToXml(testResultsIn);
        final TestResults testResultsOut = deserializeFromXml(xml);

        // Check
        assertEquals(testResultsIn.outdir, testResultsOut.outdir);
        assertEquals(testResultsIn.output, testResultsOut.output);
        assertEquals(testResultsIn.result, testResultsOut.result);
    }

    private String serializeToXml(final TestResults testResults) throws JAXBException {
        final Marshaller marshaller = jaxbContext.createMarshaller();
        final StringWriter writer = new StringWriter();
        marshaller.marshal(testResults, writer);
        return writer.toString();
    }

    private TestResults deserializeFromXml(final String xml) throws JAXBException {
        final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (TestResults) unmarshaller.unmarshal(new StringReader(xml));
    }
}
