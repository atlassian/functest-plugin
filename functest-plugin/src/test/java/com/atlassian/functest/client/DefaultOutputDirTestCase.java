package com.atlassian.functest.client;

public class DefaultOutputDirTestCase {

    static final String BASE_URL = "http://localhost";

    @RemoteTestRunner.BaseURL
    public static String base() {
        return BASE_URL;
    }

}
