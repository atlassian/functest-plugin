package com.atlassian.functest.rest;

import com.atlassian.functest.util.PluginMetaData;
import org.slf4j.Logger;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Serialize Throwable objects as HEX sequence of bytes
 * <p> It is not possible to serialize Throwable in JAXB as it includes StackTrace elements which are
 * coming without default constructor. It is quite important to send exception to the remote client as
 * if tests fails exception is the most interesting information user might want to have.
 *
 * <p> To overcome JAXB marchaling rules limitations adaptor serialize exception list in
 * a byte sequence which is possible as each Throwable must be Serializable (RMI...). Byte
 * sequence then gets encoded as HEX string.
 *
 * <p> Note that for correct work client should has access to the class information. That is
 * usually the case in case of functests
 *
 * @author pandronov
 */
public class ExceptionListAdaptor extends XmlAdapter<String, ArrayList<Throwable>> {

    private static final Logger LOG = PluginMetaData.instance().getLogger();

    /**
     * JAXB encoder for the HEX String<->Bytes conversion
     */
    private HexBinaryAdapter hexAdapter = new HexBinaryAdapter();

    /* (non-Javadoc)
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
     */
    @Override
    public String marshal(ArrayList<Throwable> list) throws Exception {
        List<Throwable> serializableList = list.stream().map(this::getSerializableThrowable).collect(Collectors.toList());

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(serializableList.toArray(new Throwable[0]));
        oos.close();
        baos.close();
        byte[] serializedBytes = baos.toByteArray();
        return hexAdapter.marshal(serializedBytes);
    }

    /* (non-Javadoc)
     * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
     */
    @Override
    public ArrayList<Throwable> unmarshal(String list) throws Exception {
        byte[] serializedBytes = hexAdapter.unmarshal(list);
        ByteArrayInputStream bais = new ByteArrayInputStream(serializedBytes);
        ObjectInputStream ois = new ObjectInputStream(bais);

        Throwable[] data = (Throwable[]) ois.readObject();
        return new ArrayList<Throwable>(Arrays.asList(data));
    }

    private Throwable getSerializableThrowable(Throwable throwable) {
        Throwable serializableThrowable = throwable;
        try {
            new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(throwable);
        } catch (IOException e) {
            LOG.warn("Failed to serialize exception, replacing...", e);
            serializableThrowable = new FunctestNonSerializableException(throwable);
        }
        return serializableThrowable;
    }
}
