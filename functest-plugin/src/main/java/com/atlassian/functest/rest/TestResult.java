package com.atlassian.functest.rest;

import junit.framework.AssertionFailedError;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.LinkedHashMap;

@XmlType
public class TestResult {
    /**
     * The overall status for all test methods {@see org.apache.tools.ant.taskdefs.optional.junit.JUnitTestRunner}
     **/
    private int status;

    /**
     * The methods tested and their status code
     **/
    public LinkedHashMap<String, TestStatus> testMethods = new LinkedHashMap<String, TestStatus>();

    /**
     * The number of tests that passed
     **/
    @XmlElement
    public int passed;

    /**
     * The number of tests failed
     **/
    @XmlElement
    public int failed;

    /**
     * The number of tests with errors
     **/
    @XmlElement
    public int errors;

    public TestResult() {
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @XmlElement
    public int getStatus() {
        return status;
    }

    public void addTest(String testCaseName) {
        testMethods.put(testCaseName, new TestStatus());
        passed++;
    }

    public void addError(String testCaseName, Throwable t) {
        testMethods.get(testCaseName).fErrors.add(t);
        errors++;
        passed--;
    }

    public void addFailure(String testCaseName, AssertionFailedError t) {
        testMethods.get(testCaseName).fFailures.add(t);
        failed++;
        passed--;
    }


    /**
     * Describes status of the single test execution
     *
     * @author pandronov
     */
    @XmlRootElement
    public static class TestStatus {

        @XmlJavaTypeAdapter(ExceptionListAdaptor.class)
        public ArrayList<AssertionError> fFailures = new ArrayList<AssertionError>();

        @XmlJavaTypeAdapter(ExceptionListAdaptor.class)
        public ArrayList<Throwable> fErrors = new ArrayList<Throwable>();

    }
}
