package com.atlassian.functest.client;

import com.atlassian.functest.client.RemoteTestRunner.Group;
import com.atlassian.functest.rest.TestDescription;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Represents remote tests group
 *
 * @author pandronov
 */
public final class GroupRunner implements Iterable<TestDescription> {
    /**
     * Local name of the tests group. Usually that is name of the method
     * annotated as a  group test
     */
    private final
    @NotNull
    String name;

    /**
     * Local description of the remote group
     */
    private final
    @NotNull
    Group grp;

    /**
     * List of tests in the group received from remote system
     */
    private final
    @NotNull
    List<TestDescription> tests;

    /**
     * Creates new local description of the remote group
     *
     * @param name  local group name
     * @param grp   remote group description
     * @param tests list of tests in the remote group
     */
    public GroupRunner(@NotNull String name, @NotNull Group grp, @NotNull List<TestDescription> tests) {
        this.name = name;
        this.grp = grp;
        this.tests = tests;
    }

    /**
     * Returns local name of the group. Usually that is name of the method
     * annotated to launch remote group
     *
     * @return local group name
     */
    @NotNull
    public String getName() {
        return name;
    }

    /**
     * Returns remote group description. Description is a group identifier along with
     * tests filter if any..
     *
     * @return remote group description
     */
    @NotNull
    public Group getGroup() {
        return grp;
    }

    /**
     * Returns iterator over the group tests.
     * <p> Order in which tests apear in the iterator is
     * not defined</p>
     *
     * @see iterator over tests in the group
     */
    @Override
    public Iterator<TestDescription> iterator() {
        return Collections.unmodifiableList(tests).iterator();
    }
}