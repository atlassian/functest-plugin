package com.atlassian.functest.junit;

import com.atlassian.functest.util.PluginMetaData;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.TestClass;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

import java.util.List;

/**
 */
public class SpringAwareJUnit4ClassRunner extends BlockJUnit4ClassRunner {


    private static ApplicationContext applicationContext;
    private static final Logger LOG = PluginMetaData.instance().getLogger();

    public static void setApplicationContext(ApplicationContext applicationContext) {
        SpringAwareJUnit4ClassRunner.applicationContext = applicationContext;
    }

    /**
     * Creates a BlockJUnit4ClassRunner to run {@code klass}
     *
     * @throws org.junit.runners.model.InitializationError if the test class is malformed.
     */
    public SpringAwareJUnit4ClassRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    protected Object createTest() throws Exception {
        LOG.info("Creating test: " + getName());

        AutowireCapableBeanFactory beanFactory = applicationContext.getAutowireCapableBeanFactory();
        TestClass testClass = getTestClass();
        return beanFactory.createBean(testClass.getJavaClass(), AutowireCapableBeanFactory.AUTOWIRE_BY_TYPE, true);
    }

    /**
     * Override default validation because spring aware test classes are expected to have single multi-argument constructor.
     * We're not calling {@link BlockJUnit4ClassRunner#validateZeroArgConstructor(List)}
     */
    @Override
    protected void validateConstructor(List<Throwable> errors) {
        validateOnlyOneConstructor(errors);
    }
}
