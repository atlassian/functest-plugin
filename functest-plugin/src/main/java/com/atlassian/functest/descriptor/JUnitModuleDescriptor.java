package com.atlassian.functest.descriptor;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.Element;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Collections2;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class JUnitModuleDescriptor extends AbstractModuleDescriptor<Void> {

    private Set<String> testPackages;
    private Set<String> excludes;

    @Inject
    public JUnitModuleDescriptor(@ComponentImport final ModuleFactory moduleFactory) {
        super(moduleFactory);
    }

    @Override
    public void init(@Nonnull Plugin plugin, @Nonnull Element element) throws PluginParseException {
        super.init(plugin, element);

        parsePackages(element);
        parseExcludes(element);
    }

    private void parsePackages(Element element) {
        final String multiPackages = element.attributeValue("packages");
        final String singlePackage = element.attributeValue("package");

        // read the nested "package" elements.
        @SuppressWarnings("unchecked")
        List<Element> packageElems = (List<Element>) element.elements("package");
        if (packageElems.size() > 0) {
            testPackages = new HashSet<>();
            for (Element elem : packageElems) {
                testPackages.add(elem.getTextTrim());
            }
        } else if (multiPackages != null) { // process "@packages"
            final Collection<String> pkgs = Collections2.transform(Arrays.asList(StringUtils.split(multiPackages, ",")), StringUtils::trim);

            testPackages = new HashSet<>(pkgs);
        } else if (singlePackage != null) { // process "@package"
            testPackages = Collections.singleton(singlePackage);
        } else {
            // this will just scan all.
            testPackages = Collections.singleton("");
        }
    }

    private void parseExcludes(Element element) {
        @SuppressWarnings("unchecked")
        List<Element> excludeElems = (List<Element>) element.elements("exclude");
        if (excludeElems.size() > 0) {
            excludes = new HashSet<>();
            for (Element elem : excludeElems) {
                excludes.add(elem.getTextTrim());
            }
        }
    }

    @Override
    public Void getModule() {
        return null;
    }

    public Set<String> getPackages() {
        return testPackages;
    }

    public Set<String> getExcludes() {
        return excludes;
    }
}
